import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<String> listOfItems = [];

  void generateListOfItems() {
    listOfItems = [];
    for (int i = 0; i < 20; i++) {
      listOfItems.add(i.toString());
      print(listOfItems.length);
    }
  }

  bool isSwitched = true;

  @override
  Widget build(BuildContext context) {
    generateListOfItems();

    return Scaffold(
        appBar: AppBar(
          title: Text('ListView and GridView'),
        ),
        body: Column(
          children: [
            Switch(
              value: isSwitched,
              onChanged: (value) {
                setState(() {
                  isSwitched = value;
                });
              },
            ),
            Expanded(
              child: isSwitched
                  ? Container(
                      child: ListView.builder(
                        itemExtent: 100,
                        itemCount: listOfItems.length,
                        itemBuilder: (context, index) {
                          return Container(
                            margin: EdgeInsets.all(20),
                            color: Colors.greenAccent,
                            child: Text(
                              listOfItems[index],
                            ),
                            alignment: Alignment.center,
                          );
                        },
                      ),
                    )
                  : Container(
                      child: GridView.builder(
                          itemCount: listOfItems.length,
                          gridDelegate:
                              SliverGridDelegateWithMaxCrossAxisExtent(
                            maxCrossAxisExtent: 150,
                            crossAxisSpacing: 10,
                            mainAxisSpacing: 10,
                          ),
                          itemBuilder: (context, index) {
                            return Container(
                              color: Colors.greenAccent,
                              child: Text(listOfItems[index]),
                              alignment: Alignment.center,
                            );
                          }),
                    ),
            ),
          ],
        ));
  }
}
